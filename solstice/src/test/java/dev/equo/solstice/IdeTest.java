/*******************************************************************************
 * Copyright (c) 2022 EquoTech, Inc. and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     EquoTech, Inc. - initial API and implementation
 *******************************************************************************/
package dev.equo.solstice;

import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.application.ApplicationException;

public class IdeTest {
	public static void main(String[] args) throws InvalidSyntaxException, ApplicationException {
		SolsticeIDE.main(args);
	}
}
