# EquoIDE for Gradle - Changelog

We adhere to the [keepachangelog](https://keepachangelog.com/en/1.0.0/) format.

## [Unreleased]
- First release to test publishing infrastructure, not meant for end users.